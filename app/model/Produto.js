Ext.define('MyApp2.model.Produto', {
    extend: 'Ext.data.Model',
    requires: ['Ext.data.identifier.Uuid'],
	config: {
        
        idProperty: 'uid',

    	identifier: {
        	type: 'uuid'
        },

        fields: [
            {name: 'uid', type: 'auto'},
            {name: 'id',  type: 'int'},
            {name: 'operadora_id', type: 'int'},
            {name: 'nome',   type: 'string'},
            {name: 'preco_venda', type: 'float'},
            {name: 'variavel', type: 'boolean'}
        ]
    }
});