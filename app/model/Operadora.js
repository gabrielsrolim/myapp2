Ext.define('MyApp2.model.Operadora', {
    extend: 'Ext.data.Model',
    requires: ['Ext.data.identifier.Uuid'],
    config: {
    	
    	idProperty: 'uid',

    	identifier: {
        	type: 'uuid'
        },

        fields: [
        	{name: 'uid', type: 'auto'},
        	{name: 'id',  type: 'int'},
            {name: 'ddd', type: 'string'},
            {name: 'nome', type: 'string'}    
        ]
    }
});