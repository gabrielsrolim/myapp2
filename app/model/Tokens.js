Ext.define('MyApp2.model.Token', {
    extend: 'Ext.data.Model',
    requires: ['Ext.data.identifier.Uuid'],
    config: {

        idProperty: 'uid',
        
    	identifier: {
        	type: 'uuid'
        },

        fields: [
            {name: 'uid', type: 'auto'},
            {name: 'accessToken', type: 'string'}           
        ]
    }
});