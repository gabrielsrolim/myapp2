Ext.define('MyApp2.store.Produtos',{
    extend: 'Ext.data.Store',

    requires: ['Ext.data.proxy.LocalStorage'],
    
    config: {

        model: 'MyApp2.model.Produto',
        storeId: 'Produtos',
        autoLoad: true,
        autoSync: true,

        proxy: {
            type: 'localstorage',
            id: 'stProdutos',

            reader: {
                type:'json',
                rootProperty:'produtos'
            }
        }
        /*
        proxy: {
            type: 'jsonp',
            reader: {
                type:'json',
                rootProperty:'produtos',
            },

            listeners:{
                exception: function( proxy, response, operation, eOpts ) {
                    if(operation.success){
                        alert( response.codResposta+'-'+response.mensagem );
                    }else{
                        alert("Erro "+operation.error);
                    }
                },
            },
        },

        listeners: {
            beforeload: function(store, operation, eOpts) {
                Ext.Viewport.setMasked({xtype:'loadmask'});
                store.getProxy().setUrl( dryad.controller.RequestManager.address.getValue() + '/produtos');
                store.getProxy().setExtraParam('accessToken', Ext.getStore('Tokens').getAt(0).get('accessToken'));
                return true;
            },
            
            load: function(store, records, successful, operation, eOpts) {
                if(operation.success){
                    if(operation.getResponse().codResposta != "0"){
                        alert( operation.getResponse().codResposta+'-'+operation.getResponse().mensagem);
                    }
                }
                Ext.Viewport.unmask();
            },
        },
        */
    }
});