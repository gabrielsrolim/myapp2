Ext.define('MyApp2.store.Tokens',{
    extend: 'Ext.data.Store',
    requires: ['Ext.data.proxy.LocalStorage'],

    config: {
        model: 'MyApp2.model.Token',
        storeId: 'Tokens',
        autoLoad: true,
        autoSync: true,

        proxy: {
            type: 'localstorage',
            id: 'stTokens'
        }
    }
});