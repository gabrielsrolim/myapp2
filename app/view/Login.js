Ext.define("MyApp2.view.Login", {
    extend: 'Ext.form.Panel',
    alias: 'widget.frmLogin',
    
    requires: [
        'Ext.Img', 
        'Ext.form.FieldSet', 
        'Ext.field.Password', 
        'Ext.field.Email'
    ],
    
    show: function() {
        Ext.getCmp('lbVersion').setHtml(
            '<div width=100%; align=center>'+this.getVersion()+'</div>'
        );
    },

    config:{
        version: null,
        itemId: 'frmLogin',

        layout: {
            type: 'vbox',
            align: 'center',
            pack: 'center'
        },

        items: [
            {
                xtype: 'spacer',
                height: 10
            },
            {
                xtype: 'image',
                src: 'resources/images/logo_snet.png',
                height: 110,
                width: 270
            },
            {
                xtype: 'spacer',
                height: 50
            },
            {
                xtype: 'fieldset',
                width: '95%',
                margin: '0 0 20 0' ,
                items: [
                    {
                        xtype: 'emailfield',
                        placeHolder: 'Email',
                        name: 'tfEmail',
                        autoCapitalize: false,
                        required: true
                    },
                    {
                        xtype: 'passwordfield',
                        placeHolder: 'Senha',
                        name: 'pfSenha',
                        required: true
                    }
                   
                ]
            }, 
                
            {
                xtype: 'button',
                name: 'btnLogar',
                action: 'getAccessToken',
                text: 'Logar',
                width: '95%',
                ui: 'action', 
                margin: '0 0 20 0'   
            },
            {
                xtype: 'label',
                id: 'lbVersion',
                cls: 'label-version'
            },
            {
                xtype: 'spacer',
                height: 50
            }
        ]
    }

 });
