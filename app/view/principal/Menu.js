Ext.define("MyApp2.view.principal.Menu", {
    extend: 'Ext.form.Panel',
    alias: 'widget.frmMenuView',

    config: {
        title: 'Recarga de telefone',
        accessToken: null,
        fullscreen: true,
        itemId: 'frmMenuView',

        layout: {
            type: 'vbox'
        },
        // defaults:{
        //     flex: 1
        // },

        items: [
            
            {                
                xtype: 'button',
                text: 'Vender recarga de telefone',
                cls: 'button-menu',
                action: 'showFrmTelefone'
            },
            // {
            //     xtype: 'button',
            //     cls: 'button-menu',
            //     text: 'Pagar contas e títulos bancários',
            //     action: 'showFrmContaMenu'
            // },
            // {
            //     xtype: 'button',
            //     cls: 'button-menu',
            //     text: 'Resumo das operações do dia',
            //     action: 'showFrmRelResumo'
            // },
            {
                xtype: 'button',
                cls: 'button-menu',
                text: 'Relatórios',
                action: 'showFrmRelPrincipal'
            },
            // {
            //     xtype: 'button',
            //     cls: 'button-menu',
            //     text: 'Seu saldo',
            //     action: 'getRelSaldoUser'
            // },
            {
                xtype: 'button',
                cls: 'button-menu',
                text: 'Atualizar Produtos',
                action: 'atualizarDadosRecarga'
            },
            {
                xtype: 'button',
                cls: 'button-menu',
                text: 'Alterar Senha',
                action: 'showFrmAlterarSenha'
            }
    
        ]
    }
});