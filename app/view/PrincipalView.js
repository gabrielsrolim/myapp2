Ext.define("MyApp2.view.PrincipalView", {
    extend: 'Ext.NavigationView',
    alias: 'widget.frmPrincipalView',

    requires: [ 
        'MyApp2.view.principal.Menu'
    ],

    config: {
        fullscreen: true,
        defaultBackButtonText: 'Voltar',
        autoDestroy: true,
 
        navigationBar: {
            items:[
                {
                    xtype: 'button',
                    iconCls: 'delete',
                    align: 'right',
                    text: 'Sair',
                    iconAlign: 'right',
                    action: 'doLogout'
                }
            ]
        },

        items: [    
            {                
                    xtype: 'frmMenuView',
                    title: 'Principal',
                    itemId: 'principalview'
            },
            {
                docked: 'bottom',
                xtype: 'titlebar',
                title: '',
                itemId: 'saldo'
            }
        ]

    },

    onBackButtonTap: function() {
        var atualizarSaldo = false;

        if(this.getActiveItem().getItemId() === 'frmComprovante'){
            atualizarSaldo = true;
        }

        this.pop(this.getPopDryadView(this.getActiveItem().getItemId()));  
        this.fireEvent('back', this);

        if(this.getActiveItem().getItemId() === 'principalview' && atualizarSaldo === true){
            console.log('atualizar');
            dryad.app.getController('Login').getSaldoRecarga();    
        }
    },

    getPopDryadView: function(itemId){
        var qtdPop = 0;
        if(itemId === 'frmComprovante'){
            qtdPop = 5;
        }else if(itemId === 'frmRelComprovante'){
            qtdPop = 2;
        }
        this.setDefaultBackButtonText('Voltar');
        return qtdPop;
    },

    getMaskSaldo: function(){
        var masked= {
                transparent: true,
                html: '<img '+
                       'src="resources/images/loadding_blue.gif" height="42" width="42" '+
                       'style="position: absolute; top: 0; left:'+Ext.Viewport.getSize().width/2+'px;"'+
                       '></img>'
            };
        return masked;
    }
    
});
