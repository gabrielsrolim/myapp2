Ext.define('MyApp2.controller.Login', {
    extend: 'Ext.app.Controller',  

    config: {
  
        routes: {
            '': 'showLogin',
            'index.html': 'showLogin'
        },

        refs: {


        //     frmUserData: {
        //         xtype: 'frmUserData',
        //         selector: 'frmUserData',
        //         autoCreate: true
        //     },

            frmLogin: {
                xtype: 'frmLogin',
                selector: 'frmLogin',
                autoCreate: false
            },

            frmPrincipalView: {
                xtype: 'frmPrincipalView',
                selector: 'frmPrincipalView',
                autoCreate: true
            },

            textSaldo: 'frmPrincipalView #saldo'
        },

        control: {

            'button[action=getAccessToken]' : {
                tap: 'getAccessToken'
            },

            'button[action=doLogout]' : {
                tap: 'doLogout'
            },
            
            'button[action=showFrmPrincipalView]' : {
                tap: 'showFrmPrincipalView'
            },

            'button[text="voltar"]' : {
                tap: 'showLog'
            }
        }
    },

     getAppVersion: function(){
        var versionDryad = new Ext.Version('0.0.5.54');
        return versionDryad;
    },

    showLogin: function() {
        
        if(this.getFrmLogin()){
            this.getFrmLogin().destroy();
        }

        if(Ext.getStore("Tokens").getData().length===0){
            var form = Ext.create('MyApp2.view.Login');
            form.setVersion(this.getAppVersion());
            Ext.Viewport.setActiveItem(form);    
        }else{
            this.showFrmPrincipalViewAccessToken();
            //this.showFrmPrincipalView();
        }

        
    },

    showLoginError: function() {
        
        if(this.getFrmLogin()){
            this.getFrmLogin().destroy();
        }

        Ext.getStore("Tokens").removeAll();
        var form = Ext.create('dryad.view.Login');
        form.setVersion(this.getAppVersion());
        Ext.Viewport.setActiveItem(form);    

        
    },

    showFrmPrincipalView: function() {
        this.getFrmPrincipalView().reset();
        Ext.Viewport.setActiveItem( this.getFrmPrincipalView() );
        this.getTextSaldo().setTitle('');
        this.getSaldoRecarga();

    },

    //Faz consulta da validade do Token antes de mostrar a tela principal.
    showFrmPrincipalViewAccessToken: function() {
        var req = dryad.util.RequestManager;
        req.doRequest("/refreshToken", { accessToken: Ext.getStore('Tokens').getAt(0).get('accessToken') },
            function(){
                var me = dryad.app.getController('Login');
                me.showFrmPrincipalView();
            }
        );
    },

    getAccessToken: function() {
        var valores = this.getFrmLogin().getValues();
        // console.log(this);
        // console.log(barcodescanner);
        cordova.plugins.barcodeScanner.scan(
            function(result) { 
                lert("We got a barcode\n" + "Result: " + result.text + "\n" + "Format: " + result.format + "\n" + "Cancelled: " + result.cancelled);
            }, 
            function(error) { 
                alert("Scanning failed: " + error); 
            }
        );
        // if( ! dryad.util.SnetUtil.validaEmail(valores.tfEmail) || (valores.pfSenha == '')){
        //     Ext.Msg.alert('Login', 'Credenciais inválidas !', Ext.emptyFn);
        // }else{
        //     var stTokens = Ext.getStore("Tokens");
        //     var req = dryad.util.RequestManager;
        //     stTokens.removeAll();
        //     req.doRequest("/login/dryad", {email: valores.tfEmail, passwd: valores.pfSenha},
        //         function(){
        //             tk = Ext.create('dryad.model.Token', {"accessToken":req.resposta.accessToken})
        //             stTokens.add(tk); 
        //             //Ext.Viewport.setActiveItem(dryad.app.getController('Recarga').showFrmTelefone());
        //             //Ext.Viewport.setActiveItem( dryad.app.getController('Login').showFrmPrincipal() );
        //             Ext.Viewport.setActiveItem( dryad.app.getController('Login').showFrmPrincipalView() );
        //         }
        //     );

        // }
    },

    getSaldoRecarga: function(){
        var req = dryad.util.RequestManager,
            saldo = 0.0,
            textSaldo = this.getTextSaldo();
        
        if(textSaldo.getTitle() === ''){
            textSaldo.setMasked(this.getFrmPrincipalView().getMaskSaldo());    
        }
        
        req.doRequest("/recarga/saldo", { accessToken: Ext.getStore('Tokens').getAt(0).get('accessToken') },
            function(){
                saldo = req.resposta.saldo;
                dryad.app.getController('Login').atualizaSaldoRecarga(saldo);
            },false
        );
        
    },

    atualizaSaldoRecarga: function(saldo){
        var textSaldo = this.getTextSaldo();
        textSaldo.setMasked(false);
        textSaldo.setTitle('Saldo: R$ '+dryad.util.SnetUtil.floatParaMoeda(saldo));
    },

    doLogout: function() {
        var req = dryad.util.RequestManager;
        req.doRequest("/logout/dryad", { accessToken: Ext.getStore('Tokens').getAt(0).get('accessToken') },
            function(){
                var me = dryad.app.getController('Login');
                var token = Ext.getStore("Tokens");
                token.removeAll();
                me.showLogin();
            }
        );
    },

    showFrmUserData: function(btn) {
        var req = dryad.util.RequestManager;
        req.doRequest("/dadosUsuario/dryad", { accessToken: Ext.getStore('Tokens').getAt(0).get('accessToken') },
            function(){
                var form = dryad.app.getController('Login').getFrmUserData();
                var saldo = dryad.util.SnetUtil.floatParaMoeda(req.resposta.saldoRecarga);
                form.setHtml(
                    '<p style="text-align: center;">'+
                    req.resposta.nomeEmpresa+'<br/>'+
                    req.resposta.nome+'<br/>'+
                    req.resposta.email+'<br/>'+
                    '<strong>Saldo R$ '+saldo+'</strong></p>'
                );

                form.showBy(btn);
            }
        );
    },

    showLog: function(){
        console.log("voltar");
    }

    
});