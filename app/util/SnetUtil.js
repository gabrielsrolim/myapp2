Ext.define('MyApp2.util.SnetUtil', {
	
	singleton: true,
	

	digitaMoeda: function (str){
		
		var decimalSeparato = ',';
		var thousandSeparator = '.';
		
		str = str.toString().replace(/\D/g,"");

		var value = parseInt(str)
		value = value / 100;
		return this.floatParaMoeda(value);
	},

	formatFone: function (value){
		v = value;
		v = v.substring (0,14);
		v=v.replace(/\D/g,"");
		v=v.replace(/(\d{0})(\d)/,"$1($2");
		v=v.replace(/(\d{2})(\d)/,"$1)$2");
		v=v.replace(/(\d{4})(\d{1,4})$/,"$1-$2");
		return v;
	},

    validaTelefone: function(numTel){
        numTel = numTel.replace(/\D/g,"");
        if (numTel.length < 10) {
            return false;
        }

        if(numTel.length > 11){
            return false;
        }

        return true;
    },

    getDdd: function (numTel) {
        numTel = numTel.replace(/\D/g,"");
        return numTel.substring(0,2);
    },

    getNumTelefone: function (numTel) {
        numTel = numTel.replace(/\D/g,"");
        return numTel.substring(2, numTel.length);
    },

    floatParaMoeda: function(num){
        x = 0;
        if(num<0) {
            num = Math.abs(num);
            x = 1;
        }
        if(isNaN(num)) num = "0";
        cents = Math.floor((num*100+0.5)%100);

        num = Math.floor((num*100+0.5)/100).toString();

        if(cents < 10) cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
            num = num.substring(0,num.length-(4*i+3))+'.'
            +num.substring(num.length-(4*i+3));
        ret = num + ',' + cents;

        if (x == 1) ret = '-' + ret;
        return ret;
    },
	
    moedaParaFloat: function(moeda){
        if(moeda){
            if(moeda.indexOf('-') != -1){
                sinal = '-';
                moeda = moeda.replace("-","");
            }else{
                sinal = '';
            }
            moeda = moeda.replace(".","");
            moeda = moeda.replace(".","");
            moeda = moeda.replace(".","");
            moeda = moeda.replace(".","");
            moeda = moeda.replace(",",".");
            moeda = parseFloat(moeda);
            moeda = new String(moeda);
            if(moeda.indexOf('.') == -1){
                moeda = moeda+'.00';
            }else{
                decimal = moeda.substr(moeda.indexOf('.')+1,2);
                if(decimal.length < 2){
                    moeda = moeda+"0";
                }
            }
            moeda = sinal+moeda;
        }

        return new Number(moeda);
    },

    validaEmail: function(email){
        var str = email;
        var filtro = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        if(filtro.test(str)) {
            return true;
        } else {
            return false;
        }
    },

    formataBlocoTitulo: function (value){
        v = value;
        v=v.replace(/\D/g,"");
        v=v.replace(/(\d{5})(\d)/,"$1.$2");
        return v;
    },

    formataBlocoConsumo: function (value){
        v = value;
        v=v.replace(/\D/g,"");
        v=v.replace(/(\d{11})(\d)/,"$1-$2");
        return v;
    },

    valida_bloco_cb4: function (digitos, bloco1){
        digitos = digitos.replace(/[^0-9]/g,'');
        var bloco = digitos.substr(0, 11);
        var digito = digitos.substr(11, 1); 
        
        if((bloco1.substr(2,1) == '6') || (bloco1.substr(2,1) == '7')){        
            return ( this.modulo10(bloco) == digito );
        } else {
            return ( this.modulo11_banco(bloco) == digito );
        }
    },

    valida_bloco_cb5: function(digitos){

        digitos = digitos.replace(/[^0-9]/g,'');
        var bloco = digitos.substr(0, digitos.length-1);
        var digito = digitos.substr(digitos.length-1, 1);
        return ( this.modulo10(bloco) == digito );
    },

    calcula_barra_cb5: function(digitos) {
        //34191.81650 63083.845576 90073.360001 4 56360000065754
        //34194563600000657541816563083845579007336000 

        barra  = digitos.replace(/[^0-9]/g,'');
        //
        // CÁLCULO DO DÍGITO DE AUTOCONFERÊNCIA (DAC)   -   5ª POSIÇÃO
        if (this.modulo11_banco('34191000000000000001753980229122525005423000') != 1) 
            throw 'Função "modulo11_banco" está com erro!';
        
        if (barra.length < 47 ) 
            barra = barra + '00000000000'.substr(0,47-barra.length);

        if (barra.length != 47) 
            throw 'A linha do Código de Barras está incompleto! '+barra.length;
        
        barra = barra.substr(0,4)
                +barra.substr(32,15)
                +barra.substr(4,5)
                +barra.substr(10,10)
                +barra.substr(21,10);

         if (this.modulo11_banco(barra.substr(0,4)+barra.substr(5,39)) != barra.substr(4,1))
            throw 'Digito verificador geral inválido';

       return barra;
    },

    calcula_linha_cb5: function(cb) {

    //03399185675290000001027671901026555380000039500
       
        linha = cb.replace(/[^0-9]/g,'');
        
        if (this.modulo10('399903512') != 8) 
            throw 'Função "modulo10" está com erro!';

        if (linha.length != 44) 
            throw 'A linha do Código de Barras está incompleta!';
        
        var campo1 = linha.substr(0,4)+linha.substr(19,1)+'.'+linha.substr(20,4);
        var campo2 = linha.substr(24,5)+'.'+linha.substr(24+5,5);
        var campo3 = linha.substr(34,5)+'.'+linha.substr(34+5,5);
        var campo4 = linha.substr(4,1);     // Digito verificador
        var campo5 = linha.substr(5,14);    // Vencimento + Valor
        
        if (  this.modulo11_banco(  linha.substr(0,4)+linha.substr(5,99)  ) != campo4 )
            throw 'Digito verificador geral inválido';
        if (campo5 == 0) campo5 = '000';
        
        linha =  campo1 + this.modulo10(campo1)
                +' '
                +campo2 + this.modulo10(campo2)
                +' '
                +campo3 + this.modulo10(campo3)
                +' '
                +campo4
                +' '
                +campo5;
              
        return linha;
    },

    calcula_barra_cb4: function(digitos){
        //digitos no formato 83640000002-9 12345678901-2 12345678901-3 12345678901-4
        //836400000029123456789012123456789013123456789014
        //83690000000220800090278901060813000119389492
        digitos = digitos.replace(/[^0-9]/g,'');
        //console.log(digitos);
        if( this.valida_dv_geral_cb4(digitos) ){
            return digitos.substr(0,11) + digitos.substr(12,11) + digitos.substr(24,11) + digitos.substr(36,11);
        }
    },

    valida_dv_geral_cb4: function (digitos){
        
        var cb;
        digitos = digitos.replace(/[^0-9]/g,'');
        
        if(digitos.length == 48) {
            cb = digitos.substr(0,11) + digitos.substr(12,11) + digitos.substr(24,11) + digitos.substr(36,11);
        } else if(digitos.length == 44) {
            cb = digitos;
        } else {
            throw "Código de barras inválido";
        }

        var dv_geral = digitos.substr(3,1);
        cb = cb.substr(0,3)+cb.substr(4);

        if( (digitos.substr(2,1) == '6') || (digitos.substr(2,1) == '7') ){        
            return ( this.modulo10(cb) == dv_geral );
        } else {
            return ( this.modulo11_banco(cb) == dv_geral );
        }
    }, 

    valida_dv_geral_cb5: function(digitos){
        if(digitos.length == 44){
            digitos = this.calcula_linha_cb5(digitos);
        }
        var cb = this.calcula_barra_cb5(digitos);
        return cb.length == 44;
    },

    he5Blocos: function (cb){
        return (this.modulo11_banco(cb.substr(0,4)+cb.substr(5,39)) == cb.substr(4,1));
    },

    get_segmento: function (cb){
   
        cb = cb.replace(/[^0-9]/g,'');
        
        if(this.he5Blocos(cb)){
            return 0;
        }else{
            switch (cb.substr(1,1)) {

                case '1':
                    return 'Prefeituras';
                case '2':
                    return 'Saneamento';
                case '3':
                    return 'Energia';
                case '4':
                    return 'Telecomunicações';
                case '5':
                    return 'Orgãos Governamentais';
                case '6':
                    return 'Carnês e Assemelhados ou demais';
                case '7':
                    return 'Multas de Transito';
                case '9':
                    return 'Uso exclusivo do banco';
                default:'0'
                    return '0';
            }
        }
    },

    fator_vencimento: function (dias) {
        //Fator contado a partir da data base 07/10/1997
        //*** Ex: 04/07/2000 fator igual a = 1001
        //alert(dias);
        var currentDate, t, d, mes;
        t = new Date();
        currentDate = new Date();
        currentDate.setFullYear(1997,9,7);
        t.setTime(currentDate.getTime() + (1000 * 60 * 60 * 24 * dias));
        
        return (t.getDate()+'/'+(t.getMonth()+1)+'/'+t.getFullYear());
     
    },

    get_vencimento: function (cb) {
   
        cb = cb.replace(/[^0-9]/g,'');
        
        if(this.he5Blocos(cb)){

            if ( cb.substr(5,4) == 0 ){
                return null;
            } else {
                return this.fator_vencimento(cb.substr(5,4));
            }

         }else{
            return null;
         } 
    },

    get_valor: function(cb) {
        //34194563600000657541816563083845579007336000
        //83690000000220800090278901060813000119389492
        cb = cb.replace(/[^0-9]/g,'');
        
        if(this.he5Blocos(cb)){    
            return cb.substr(9,10)/100;      
        }else{
            if(this.get_moeda(cb) == '6' || this.get_moeda(cb) == '8'){
                return  cb.substr(4,11)/100 ;
            }else{
                return  cb.substr(4,11)+"00"/100 ;    
            }
        }
    },

    get_moeda: function(cb) {

        cb = cb.replace(/[^0-9]/g,'');
        if(this.he5Blocos(cb)){
            return (cb.substr(3,1));    
        }else{
            return (cb.substr(2,1));
        }         
    },

    get_inicio_cb: function(cb) {
        cb = cb.replace(/[^0-9]/g,'');
        return (cb.substr(0,3));   
    },

    modulo10: function(numero) {
        numero = numero.replace(/[^0-9]/g,'');
        var soma  = 0;
        var peso  = 2;
        var contador = numero.length-1;
       
        while (contador >= 0) {
            
            multiplicacao = ( numero.substr(contador,1) * peso );
            if (multiplicacao >= 10) {multiplicacao = 1 + (multiplicacao-10);}
            soma = soma + multiplicacao;
            
            if (peso == 2) {
                peso = 1;
            } else {
                peso = 2;
            }
            contador = contador - 1;
        }
        var digito = 10 - (soma % 10);
        
        if (digito == 10) digito = 0;
        return digito;
    },

    modulo11_banco: function(numero) {
        numero = numero.replace(/[^0-9]/g,'');
        
        var soma  = 0;
        var peso  = 2;
        var base  = 9;
        var resto = 0;
        var contador = numero.length - 1;
        
        for (var i=contador; i >= 0; i--) {
            
            soma = soma + ( numero.substring(i,i+1) * peso);
           
            if (peso < base) {
                peso++;
            } else {
                peso = 2;
            }
        }
        var digito = 11 - (soma % 11);
        
        if (digito >  9) digito = 0;
        /* Utilizar o dígito 1(um) sempre que o resultado do cálculo padrão for igual a 0(zero), 1(um) ou 10(dez). */
        if (digito == 0) digito = 1;
        return digito;
    }
});