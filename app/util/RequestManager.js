Ext.define('MyApp2.util.RequestManager', {

    requires: [
        'Ext.data.JsonP',
        'MyApp2.util.RequestPostProcessor',
        'Ext.device.Connection'
    ],
   
    singleton: true,

    address: {
        host: 'https://eos.servicenet.com.br',
        port: 8888,

        //host: 'http://192.168.42.29',
        //host: 'http://192.168.42.33',
        //port: 8888,

        getValue: function(){
            return this.host+':'+this.port;
        }
    },

    resposta: null ,

    doRequest: function(url, params, callback, loadMascara){

        if(loadMascara !== false){
            loadMascara = true;
        }
       
        if ( ! Ext.device.Connection.isOnline()) {
            Ext.Msg.alert('MyApp2', "Sem conexão !!", Ext.emptyFn);
            return false;
        }
        
        if(loadMascara){
            Ext.Viewport.setMasked({xtype:'loadmask'});
        }

        Ext.data.JsonP.request({
            url : this.address.getValue() + url,
            params: params,
            scope: this,
            timeout: 65000,

            success: function(resp) {
                 try{
                    this.resposta = resp;
                    if(resp.codResposta != "0") {
                        Ext.Msg.alert('MyApp2', this.resposta.codResposta+'-'+this.resposta.mensagem , Ext.emptyFn);
                    } else {
                        callback.call(this);  
                    }  
                }catch( e ){
                    Ext.Msg.alert( 'Erro', e.toString() , Ext.emptyFn);
                }finally{
                    MyApp2.util.RequestPostProcessor.doAction(resp);
                    if(loadMascara){
                        Ext.Viewport.unmask();
                    }
                }
            },

            failure: function(resp) {
                try{
    
                    if(resp = 'error'){
                        throw new Error( 'Recurso não encontrado '+url );
                    }
                
                    this.resposta = resp ;
                    throw new Error(this.resposta.codResposta+'-'+this.resposta.mensagem );
                }catch(e){
                    Ext.Msg.alert( 'MyApp2', e.toString() , Ext.emptyFn);
                } finally{
                    MyApp2.util.RequestPostProcessor.doAction(resp);
                    if(loadMascara){
                        Ext.Viewport.unmask();
                    }
                }
            }           
        });   
    }
});