Ext.define('MyApp2.util.RequestPostProcessor', {
   
    singleton: true,

    doAction: function(resp){

        if(resp.codResposta === "N10"){
            MyApp2.app.getController('Login').showLoginError();
        }else if (resp.codResposta === "N11"){
        	MyApp2.app.getController('Login').showLoginError();
        } else if (resp.codResposta === "N21"){
        	MyApp2.app.getController('Login').showLoginError();
        } else if (resp.codResposta === "N23"){
            MyApp2.app.getController('Recarga').apagarDadosRecarga();
            MyApp2.app.getController('Recarga').getFrmTelefone().reset();
            MyApp2.app.getController('Recarga').getFrmPrincipalView().pop(3)
            //MyApp2.app.getController('Recarga').showFrmTelefone();
        } else if(resp.atualizarProdutos){
            MyApp2.app.getController('Recarga').apagarDadosRecarga();
        }
    }

});